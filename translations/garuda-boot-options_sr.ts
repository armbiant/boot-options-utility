<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr">
<context>
    <name>CustomDialog</name>
    <message>
        <location filename="../dialog.cpp" line="15"/>
        <source>Live environment detected. Please select the root partition of the
 system you want to modify (only Linux partitions are displayed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="22"/>
        <source>OK</source>
        <translation>У реду</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="23"/>
        <source>Cancel</source>
        <translation>Одустани</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="32"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="38"/>
        <source>Kernel parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="58"/>
        <source>Set to &apos;0&apos; to boot immediately without displaying the menu, or to &apos;-1&apos; to wait indefinitely</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="61"/>
        <source>Menu timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="74"/>
        <source>Boot to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="81"/>
        <source>Use simplified menu structure without submenus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="84"/>
        <source>Use flat menus (no submenus)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="113"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="120"/>
        <source>With this option enabled whatever entry you select from the grub boot menu will be saved as the new default for future boots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="123"/>
        <source>Enable saving last boot choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="136"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="154"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="167"/>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Enable theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="247"/>
        <source>Splash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="282"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="305"/>
        <source>Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="311"/>
        <source>Very detailed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="318"/>
        <source>Detailed (default setting)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="328"/>
        <source>Limited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="344"/>
        <source>Display log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="397"/>
        <source>Display help </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="400"/>
        <source>Help</source>
        <translation>Помоћ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Alt+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="439"/>
        <source>About this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="442"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="449"/>
        <source>Alt+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="484"/>
        <source>Quit application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="487"/>
        <location filename="../mainwindow.cpp" line="684"/>
        <source>Close</source>
        <translation>Близу</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="494"/>
        <source>Alt+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="535"/>
        <source>Apply</source>
        <translation>Потврди</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="74"/>
        <source>Still running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="168"/>
        <source>Installing bootsplash, please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Updating sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="177"/>
        <source>Installing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Error</source>
        <translation>Грешка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="181"/>
        <source>Could not install the bootsplash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <location filename="../mainwindow.cpp" line="329"/>
        <source>Cannot continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <source>Nothing was selected, cannot change boot options. Exiting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="329"/>
        <source>Cannot create chroot environment, cannot change boot options. Exiting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="576"/>
        <location filename="../mainwindow.cpp" line="887"/>
        <source>Updating configuration, please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="627"/>
        <source>Updating initramfs...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="636"/>
        <location filename="../mainwindow.cpp" line="900"/>
        <source>Updating grub...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="639"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="639"/>
        <source>Changes have been successfully applied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="652"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="653"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="654"/>
        <source>Program for selecting common start-up choices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="656"/>
        <source>Copyright (c) Garuda Linux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="657"/>
        <location filename="../mainwindow.cpp" line="667"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="658"/>
        <location filename="../mainwindow.cpp" line="673"/>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="659"/>
        <source>Cancel</source>
        <translation>Одустани</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="703"/>
        <source>Garuda Boot Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="715"/>
        <source>Running in a Virtual Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="716"/>
        <source>You current system is running in a Virtual Machine,
Plymouth bootsplash will work in a limited way, you also won&apos;t be able to preview the theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="721"/>
        <source>Plymouth packages not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="721"/>
        <source>Plymouth packages are not currently installed.
OK to go ahead and install them?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <source>Images (*.png *.jpg *.jpeg *.tga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="850"/>
        <source>Press any key to close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="852"/>
        <source>Log not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="852"/>
        <source>Could not find log at </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="866"/>
        <source>Needs reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="866"/>
        <source>Plymouth was just installed, you might need to reboot before being able to display previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="922"/>
        <source>Click to select theme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="58"/>
        <source>You must run this program as root.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="741"/>
        <source>Select image to display in bootloader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="931"/>
        <source>Select GRUB theme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
